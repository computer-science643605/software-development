
# # Output to Word document
# with open('output_word_file.txt', 'w') as file:
#     file.write(f'CGPA Result: {cgpa_result}\n')
#     file.write(f'Basic Calculator Result: {basic_calculator_result}\n')


def basic_calculator():
    calculate_sum=sum([2300725926,2300708403,2300713145,2300700226])
    return calculate_sum
number_str=str(basic_calculator())
list_marks=[]
for i in range(0,len(number_str)-1,2):
    mark=number_str[i:i+2]
    list_marks.append(mark)
list_mark=[]
for mar in list_marks:
    mar=int(mar)
    list_mark.append(mar)
marks=list_mark[1:]
def calculate_grade_point(mark):
    if mark >=80:
       return 5.0
    elif mark >=75:
        return 4.5
    elif mark >=70:
        return 4.0
    elif mark >= 65:
        return 3.5
    elif mark >=60:
        return 3.0
    elif mark >=55:
        return 2.5
    elif mark >=50:
        return 2.0
    elif mark >=45:
        return 1.5
    elif mark >=40:
        return 1.0
    elif mark <= 39:
        return 0
def cgpa_calculator(marks,credits):
    total_grade_point =0
    total_credit=0
    for i in range (len(marks)):
        mark=marks[i]
        cred=credits[i]
        grade_point=calculate_grade_point(mark)
        grade_point_credit=grade_point*cred
        total_grade_point+=grade_point_credit
        total_credit+=cred
        cgpa=total_grade_point/total_credit
    return cgpa
credits=[4,4,4,4]   
marks=marks 
result_cgpa=cgpa_calculator(marks,credits)
from docx import Document
document=Document()  
document.add_paragraph(f'basic_calculator:{basic_calculator()}')
document.add_paragraph(f'Cgpa_calculator: {result_cgpa}')
document.save('Result.docx')