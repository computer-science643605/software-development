from django.template import loader
from django.http import HttpResponse
from .models import member

def members(request):
    mymembers=member.objects.all().values()
    template=loader.get_template('all_members.html')
    context={'mymembers':mymembers,}
    return HttpResponse(template.render(context,request))

def details(request, id):
    mymember=member.objects.all().values()
    template=loader.get_template('details.html')
    context={'mymember':mymember,}
    return HttpResponse(template.render(context,request))