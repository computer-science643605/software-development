from django.contrib import admin
from .models import member


class memberAdmin(admin.ModelAdmin):
    list_display=("firstname","lastname","joined_date",)
    
admin.site.register(member,memberAdmin)
