from django.urls import path
from . import views
urlpatterns = [
    path('hello/', views.members, name='members'),
    path('hello/details/<int:id>', views.details, name='details'),
               ]
