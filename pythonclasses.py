import datetime
class Employee:
    number_of_employees=0
    raise_amount=2.5
    def __init__(self,first,last,pay):
        self.first=first
        self.last=last
        self.pay=pay
        self.email=first+ '.'+ last+'@gmail.com'
        Employee.number_of_employees+=1
    def full_name(self):
        return '{} {}'.format(self.first,self.last)
    def pay_changes(self):
        return (self.pay*Employee.raise_amount)
    @classmethod
    def set_raise_amount(cls,amount):
        cls.raise_amount=amount
    @classmethod
    def split_user(cls,employee):
        first,last,amount=employee.split('_')
        return cls(first,last,int(amount))
    @staticmethod
    def work_days(day):
        if day.weekday()==5 or day.weekday()==6:
            return False
        return True
emp='Nakabaale_Benjamin_1000000'
emp_1=Employee.split_user(emp)
Employee.set_raise_amount(1.5)

time=datetime.datetime.today()
print(emp_1.full_name())
print(emp_1.pay_changes())
print(Employee.number_of_employees)
print(Employee.work_days(time))
print(time)
